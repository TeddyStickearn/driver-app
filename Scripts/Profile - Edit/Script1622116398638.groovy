import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\Teddy\\Desktop\\a12a9ed50c1c3b39d1c08a128ddcf977d059937436dc6595c759934e15142bc9.apk', 
    true)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.Button - Allow'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - OR LOGIN WITH EMAIL'), 0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - Alamat Email'), 'datalengkap@mail.com', 
    0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - Kata Sandi'), '1234567', 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.Button - MASUK'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - Akun'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - Detail'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - Ubah'), 0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - DATA LENGKAP'), 'Data Edited', 
    0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - 81375283262'), '81213131413', 
    0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - Surabaya'), 0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - Ketik disini untuk mencari'), 
    'jabodetabek', 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - Jabodetabek'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.Button - SIMPAN'), 0)

Mobile.pressBack()

Mobile.getText(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - Data Edited'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.TextView - Ubah'), 0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - Data Edited'), 'DATA LENGKAP', 
    0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - 81213131413'), '81375283262', 
    0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - Jabodetabek'), 0)

Mobile.setText(findTestObject('Profile - Edit/android.widget.EditText - Ketik disini untuk mencari'), 'surabaya', 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - Surabaya'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.Button - SIMPAN'), 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - KENDARAAN'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.TextView - Ubah'), 0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - B 22 B'), 'B 99 Q', 0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - 2001'), '2002', 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - Silver'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - Abu Abu'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - Toyota Avanza'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - Toyota Agya'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.Button - SIMPAN'), 0)

Mobile.pressBack()

Mobile.getText(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - B 99 Q'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.TextView - Ubah'), 0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - B 99 Q'), 'B 22 B', 0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - 2002'), '2001', 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - Toyota Agya'), 0)

Mobile.setText(findTestObject('Profile - Edit/android.widget.EditText - Ketik disini untuk mencari'), 'avanza', 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - Toyota Avanza'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.EditText - Abu Abu'), 0)

Mobile.setText(findTestObject('Profile - Edit/android.widget.EditText - Ketik disini untuk mencari'), 'silver', 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - Silver'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.Button - SIMPAN'), 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - BANK'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.TextView - Ubah'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.EditText - American Express Bank Ltd'), 0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - Ketik disini untuk mencari (1)'), 
    'panin', 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - Bank Panin'), 0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - 764528287456821 (1)'), '764528287456822', 
    0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - DATA (1)'), 'edited', 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - Diri Sendiri (1)'), 0)

Mobile.setText(findTestObject('Profile - Edit/android.widget.EditText - Ketik disini untuk mencari'), 'ayah', 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.TextView - Ayah'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.Button - SIMPAN (2)'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.Button - OK'), 0)

Mobile.pressBack()

Mobile.getText(findTestObject('Profile - Edit/android.widget.TextView - Ayah'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.TextView - Ubah'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - Bank Panin'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.EditText - American Express Bank Ltd'), 0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - 764528287456822'), '764528287456821', 
    0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - edited'), 'DATA', 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.EditText - Ayah'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.TextView - Diri Sendiri'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.Button - SIMPAN (2)'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.Button - OK'), 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - BERKAS'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.TextView - Ubah'), 0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - 7645282874568241'), '7645282874568242', 
    0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.KTP'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - Buka Kamera'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.ImageView-RotateCam'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.ImageView-Flash'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.ImageView-TakePhoto'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.Button - LANJUT'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.Button - Selanjutnya'), 0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - 764528287456822 (1)'), '764528287456821', 
    0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.ImageView-SIM'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.TextView - Buka Kamera'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.ImageView-TakePhoto'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.Button - LANJUT'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.Button - Selanjutnya (1)'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.ImageView-STNK'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.TextView - Buka Kamera'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.ImageView-TakePhoto'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - Edit/android.widget.Button - RETAKE PHOTO'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.ImageView-TakePhoto'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.Button - LANJUT'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.Button - SIMPAN (2)'), 0)

Mobile.pressBack()

Mobile.getText(findTestObject('Object Repository/Profile - Edit/android.widget.TextView - 7645282874568242'), 0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.TextView - Ubah'), 0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - 7645282874568242'), '7645282874568241', 
    0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.Button - Selanjutnya'), 0)

Mobile.setText(findTestObject('Object Repository/Profile - Edit/android.widget.EditText - 764528287456821 (2)'), '764528287456822', 
    0)

Mobile.tap(findTestObject('Profile - Edit/android.widget.Button - SIMPAN (2)'), 0)

Mobile.pressBack()

Mobile.pressBack()

Mobile.closeApplication()

