import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\Teddy\\Desktop\\a12a9ed50c1c3b39d1c08a128ddcf977d059937436dc6595c759934e15142bc9.apk', 
    false)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - Akun'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - Riwayat Iklan'), 0)

Mobile.setText(findTestObject('Object Repository/Profile - General/android.widget.EditText - Ketik disini untuk mencari'), 
    'test 2 mar', 0)

Mobile.getText(findTestObject('Object Repository/Profile - General/android.widget.TextView - test 2 mar'), 0)

Mobile.setText(findTestObject('Object Repository/Profile - General/android.widget.EditText - test 2 mar'), 'not found anything', 
    0)

Mobile.clearText(findTestObject('Object Repository/Profile - General/android.widget.EditText - not found anything'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - Aktif'), 0)

Mobile.getText(findTestObject('Object Repository/Profile - General/android.widget.TextView - AKTIF (1)'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - Selesai'), 0)

Mobile.getText(findTestObject('Object Repository/Profile - General/android.widget.TextView - SELESAI (1)'), 0)

Mobile.tap(findTestObject('Profile - General/android.widget.BackButton'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - Kontrak'), 0)

Mobile.tap(findTestObject('Profile - General/android.widget.TextView - Surat Jalan (1)'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - Panduan Aplikasi'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - 0'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - Pelaporan'), 0)

Mobile.tap(findTestObject('Profile - General/android.widget.SubmitButton'), 0)

Mobile.tapAndHold(findTestObject('Profile - General/android.widget.TextView - Surat Jalan (1)'), 0, 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - Alarm Pelaporan'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.RadioButton - 2 jam'), 0)

Mobile.tap(findTestObject('Profile - General/android.widget.TextView - Alarm Pelaporan'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.RadioButton - 3 jam'), 0)

Mobile.tap(findTestObject('Profile - General/android.widget.TextView - Alarm Pelaporan'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.RadioButton - 5 jam'), 0)

Mobile.tap(findTestObject('Profile - General/android.widget.TextView - Alarm Pelaporan'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.RadioButton - 1 jam'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - Diagnostik'), 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - Pengembalian Aset'), 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Profile - General/android.widget.TextView - Ubah Bahasa'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.RadioButton - Chinese'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - SELESAI (2)'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - ChangeLanguageChinese'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.RadioButton - English'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - YesChinese'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - Change Language'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.RadioButton - Filipino'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - DONE'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - pagbabago ng wika'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.RadioButton - Malay'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - TAPOS NA'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - Tukar Bahasa'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.RadioButton - Myanmar'), 0)

Mobile.tap(findTestObject('Profile - General/android.widget.TextView - SELESAI (2)'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - ChangeLanguageMyanmar'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.RadioButton - Thai'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - YesMyanmar'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - ChangeLanguageThai'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.RadioButton - Vietnamese'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - YesThai'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - thay i ngn ng'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.RadioButton - Bahasa'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - LM XONG'), 0)

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - Nilai Stickearn'), 0)

Mobile.pressBack()

Mobile.scrollToText('Keluar')

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - Privacy Policy'), 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - F.A.Q'), 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - Hubungi Kami'), 0)

Mobile.pressBack()

Mobile.pressBack()

Mobile.pressBack()

Mobile.tap(findTestObject('Object Repository/Profile - General/android.widget.TextView - Chat Dengan Kami'), 0)

