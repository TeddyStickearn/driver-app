<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Tester add the drivertesting1@gmail.com to new campaign</description>
   <name>Regression Testing - Phase 1</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>bfc15f86-3190-4d08-ad9f-2f6fd956b4b7</testSuiteGuid>
   <testCaseLink>
      <guid>1f1c50d2-74fc-49de-a405-05c1e5f60521</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login - Using Email and Password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>497aae26-cc0c-4100-aa80-ac3b225b9006</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home - General</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>780db05e-a5d2-436f-8625-91ce73c523fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile - General</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36dfd041-b579-40a8-92c4-50c064c6eee6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Report General</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef73b563-17d9-4dc7-8cd0-5b491369059e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Stickmart - General</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce426d47-9363-4b64-913b-a69bd3e6efc2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Report - Send Submission</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
