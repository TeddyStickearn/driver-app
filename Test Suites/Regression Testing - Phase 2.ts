<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Tester reject the evaluation on vendor apps</description>
   <name>Regression Testing - Phase 2</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>52da7a77-c746-4079-8c63-f2e98604e41c</testSuiteGuid>
   <testCaseLink>
      <guid>b8f2e374-7000-4e26-9456-be57f9891433</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Message - General</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5246a4b-aa8f-4710-9389-3877e13bb11b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile - Edit</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
